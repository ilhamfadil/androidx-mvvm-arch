package com.example.android.architecture.blueprints.todoapp.data.source.remote

import com.example.android.architecture.blueprints.todoapp.data.model.CartModel
import com.example.android.architecture.blueprints.todoapp.data.source.DataSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import com.example.android.architecture.blueprints.todoapp.utils.base.BaseApiModel

/**
 * Created by radhikayusuf on 17/11/2018.
 */
object RemoteDataSource : DataSource {

    private val mApiService: ApiService by lazy {
        ApiService.getApiService
    }

    override fun getListCart(refresh: Boolean, callback: DataSource.GetCartCallback) {
        mApiService.getListCart()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiCallback<BaseApiModel<List<CartModel>?>>() {
                override fun onSuccess(model: BaseApiModel<List<CartModel>?>) {
                    if (model.data != null) {
                        if (model.data.isNotEmpty()) {
                            callback.onSuccess(model.data)
                        } else {
                            callback.onError(model.message)
                        }
                    } else {
                        callback.onError(model.message)
                    }
                }

                override fun onFailure(code: Int, errorMessage: String) {
                    callback.onError(errorMessage)
                }

                override fun onFinish() {
                    callback.onFinish()
                }

            })
    }

    override fun saveListCart(data: List<CartModel>) {

    }


}