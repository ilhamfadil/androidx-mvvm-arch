package com.example.android.architecture.blueprints.todoapp.utils.chocohelper

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class ChocoRepository