package com.example.android.architecture.blueprints.todoapp.utils.base

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import com.example.android.architecture.blueprints.todoapp.data.source.Repository
import com.example.android.architecture.blueprints.todoapp.utils.chocohelper.ChocoChips
import com.example.android.architecture.blueprints.todoapp.utils.chocohelper.ChocoRepository
import com.example.android.architecture.blueprints.todoapp.utils.helper.SingleLiveEvent

/**
 * @author radhikayusuf.
 */

open class BaseViewModel(application: Application) : AndroidViewModel(application) {

    @ChocoRepository
    lateinit var mRepository: Repository

    val isRequesting = ObservableField(true)
    val showMessage = SingleLiveEvent<String>()
    val showMessageRes = SingleLiveEvent<Int>()

    open fun start(){
        ChocoChips.inject(this)
    }


}