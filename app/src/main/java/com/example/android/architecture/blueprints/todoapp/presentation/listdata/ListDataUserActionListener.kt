package com.example.android.architecture.blueprints.todoapp.presentation.listdata;

import com.example.android.architecture.blueprints.todoapp.utils.base.BaseUserActionListener

interface ListDataUserActionListener : BaseUserActionListener {

    fun onClickItem(data: ListDataModel)
    fun addNewItem()

}