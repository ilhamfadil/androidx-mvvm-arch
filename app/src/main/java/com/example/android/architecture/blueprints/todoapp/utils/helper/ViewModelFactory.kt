package com.example.android.architecture.blueprints.todoapp.utils.helper

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.android.architecture.blueprints.todoapp.presentation.home.HomeViewModel
import com.example.android.architecture.blueprints.todoapp.presentation.listdata.ListDataViewModel

/**
 * Created by radhikayusuf on 17/11/2018.
 */

class ViewModelFactory private constructor(
        private val mApplication: Application
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>) =
            with(modelClass) {
                when {
                    isAssignableFrom(ListDataViewModel::class.java) -> ListDataViewModel(mApplication)
                    isAssignableFrom(HomeViewModel::class.java)     -> HomeViewModel(mApplication)
                    else ->
                        throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
                }
            } as T

    companion object {

        @Volatile
        private var INSTANCE: ViewModelFactory? = null

        fun getInstance(mApplication: Application) =
                INSTANCE ?: synchronized(ViewModelFactory::class.java) {
                    INSTANCE ?: ViewModelFactory(mApplication)
                            .also { INSTANCE = it }
                }
    }
}