package com.example.android.architecture.blueprints.todoapp.presentation.home;

import android.app.Application
import androidx.databinding.BaseObservable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.example.android.architecture.blueprints.todoapp.utils.base.BaseViewModel

class HomeViewModel(context: Application) : BaseViewModel(context) {

    val buttonText = ObservableField("Open List Data")

}