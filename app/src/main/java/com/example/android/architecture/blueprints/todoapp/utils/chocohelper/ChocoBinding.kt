package com.example.android.architecture.blueprints.todoapp.utils.chocohelper


/**
 * Made with ❤ by Radhika Yusuf
 */


@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class ChocoBinding (val layout: Int)