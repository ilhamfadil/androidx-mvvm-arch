package com.example.android.architecture.blueprints.todoapp.presentation.listdata


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.android.architecture.blueprints.todoapp.databinding.ItemListDataBinding

class ListDataAdapter(var mData: List<ListDataModel>, val mViewModel: ListDataViewModel) : RecyclerView.Adapter<ListDataAdapter.ListDataItem>() {


    override fun onBindViewHolder(holder: ListDataItem, position: Int) {
        holder.bind(mData[position], mViewModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListDataItem {
        val binding = ItemListDataBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ListDataItem(binding)
    }

    override fun getItemCount(): Int {
        return mData.size
    }


    fun replaceData(data: List<ListDataModel>) {
        mData = data
        notifyDataSetChanged()
    }


    class ListDataItem(val mBinding: ItemListDataBinding) : RecyclerView.ViewHolder(mBinding.root) {

        fun bind(data: ListDataModel, viewModel: ListDataViewModel) {
            mBinding.mData = data
            mBinding.mListener = object : ListDataUserActionListener {

                override fun onClickItem(data: ListDataModel) {
                    viewModel.eventClickItem.value = data
                }

                override fun onRefreshPage() {

                }

            }
        }

    }

}