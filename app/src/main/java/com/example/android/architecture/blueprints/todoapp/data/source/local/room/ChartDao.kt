package com.example.android.architecture.blueprints.todoapp.data.source.local.room

import androidx.room.*
import com.example.android.architecture.blueprints.todoapp.data.model.CartModel

@Dao
interface ChartDao {

    @Query("SELECT * FROM chart")
    fun retrievedChart(): List<CartModel>?

    @Query("DELETE FROM chart")
    fun deleteAllChart()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertChart(data: CartModel)
}