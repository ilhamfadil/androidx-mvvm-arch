@file:Suppress("UNCHECKED_CAST")

package com.example.android.architecture.blueprints.todoapp.utils.chocohelper

import android.util.Log
import androidx.lifecycle.ViewModel
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.example.android.architecture.blueprints.todoapp.data.source.DataSource
import com.example.android.architecture.blueprints.todoapp.data.source.Repository
import com.example.android.architecture.blueprints.todoapp.data.source.local.LocalDataSource
import com.example.android.architecture.blueprints.todoapp.data.source.remote.RemoteDataSource
import com.example.android.architecture.blueprints.todoapp.utils.base.BaseFragment
import com.example.android.architecture.blueprints.todoapp.utils.base.BaseUserActionListener
import com.example.android.architecture.blueprints.todoapp.utils.base.BaseViewModel
import com.example.android.architecture.blueprints.todoapp.utils.extension.obtainViewModel

/**
 * Made with ❤ by Radhika Yusuf
 */


object ChocoChips {

    private var localDataSource: DataSource? = null
    private var remoteDataSource: DataSource? = null


    @JvmStatic
    fun inject(vm: BaseViewModel) {
        val myClass = vm::class.java
        for (field in myClass.superclass?.declaredFields ?: emptyArray()) {
            val annotation = field.getAnnotation(ChocoRepository::class.java)
            if (annotation != null) {
                if (localDataSource == null && remoteDataSource == null) {
                    localDataSource = LocalDataSource(vm.getApplication())
                    remoteDataSource = RemoteDataSource
                }

                field.isAccessible = true
                field.set(vm, Repository(remoteDataSource!!, localDataSource!!))
                field.isAccessible = false
            }
        }
    }

    @JvmStatic
    fun <T : ViewDataBinding, VM : BaseViewModel, L : BaseUserActionListener> inject(fragment: BaseFragment<VM>) {
        var mBinding: ViewDataBinding? = null
        var mVM: ViewModel? = null

        val myClass = fragment::class.java
        for (field in myClass.declaredFields) {
            Log.d("Field Name : ", field.name)
            val annotation = field.getAnnotation(ChocoBinding::class.java)
            if (annotation != null) {
                mBinding = DataBindingUtil.inflate<T>(LayoutInflater.from(fragment.requireActivity()), annotation.layout, null, false)
                field.isAccessible = true
                field.set(fragment, mBinding)
                field.isAccessible = false
                Log.d("Field Data Binding : ", "Found")
            }

            val vmAnnotation = field.getAnnotation(ChocoViewModel::class.java)
            if (vmAnnotation != null) {
                if (ViewModel::class.java.isAssignableFrom(field.type)) {
                    mVM = fragment.obtainViewModel(field.type as Class<VM>)
                    field.isAccessible = true
                    field.set(fragment, mVM)
                    field.isAccessible = false
                    fragment.mParentVM = mVM
                    Log.d("Field Viewmodel : ", "Found")
                }
            }
        }


        if (mBinding != null && mVM != null) {
            val classBinding = mBinding::class.java
            for (bindingField in classBinding.declaredFields) {
                Log.d("Binding Name : ", bindingField.name)
                if (ViewModel::class.java.isAssignableFrom(bindingField.type)) {
                    bindingField.isAccessible = true
                    bindingField.set(mBinding, mVM)
                    bindingField.isAccessible = false
                    Log.d("View Model : ",                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  "Found")
                } else if (BaseUserActionListener::class.java.isAssignableFrom(bindingField.type) && BaseUserActionListener::class.java.isAssignableFrom(fragment::class.java)) {
                    bindingField.isAccessible = true
                    bindingField.set(mBinding, fragment as L)
                    bindingField.isAccessible = false
                    Log.d("View Action Listener : ", "Found")
                }
            }
        }
    }


}