package com.example.android.architecture.blueprints.todoapp.utils.constant

object DefaultValue {

    val GLIDE_FADE_ANIMATION_DURATION = 500

}