package com.example.android.architecture.blueprints.todoapp.data.source

import com.example.android.architecture.blueprints.todoapp.data.model.CartModel

interface DataSource {


    fun getListCart(
        refresh: Boolean = false,
        callback: GetCartCallback
    )

    fun saveListCart(data: List<CartModel>)

    interface GetCartCallback {

        fun onSuccess(data: List<CartModel>)

        fun onError(errorMessage: String)

        fun onFinish()
    }

}