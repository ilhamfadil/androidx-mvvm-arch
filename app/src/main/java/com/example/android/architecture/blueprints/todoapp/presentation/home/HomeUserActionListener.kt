package com.example.android.architecture.blueprints.todoapp.presentation.home;

import com.example.android.architecture.blueprints.todoapp.utils.base.BaseUserActionListener

interface HomeUserActionListener : BaseUserActionListener {

    fun onClickTest()

}