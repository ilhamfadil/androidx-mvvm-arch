package com.example.android.architecture.blueprints.todoapp.utils.base

import android.annotation.SuppressLint
import androidx.recyclerview.widget.RecyclerView
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.android.architecture.blueprints.todoapp.utils.constant.DefaultValue.GLIDE_FADE_ANIMATION_DURATION

object BaseBindings {

    @BindingAdapter("app:refreshing")
    @JvmStatic
    fun setRefreshing(swipeRefreshLayout: SwipeRefreshLayout, data: Boolean) {
        swipeRefreshLayout.isRefreshing = data
    }

    @SuppressLint("PrivateResource")
    @BindingAdapter("app:imageUrl")
    @JvmStatic
    fun setImageUrl(view: ImageView, imageUrl: String?) {
        Glide.with(view.context)
            .load(imageUrl)
            .transition(
                DrawableTransitionOptions.withCrossFade(
                    GLIDE_FADE_ANIMATION_DURATION
                )
            )
            .into(view)
    }

}