package com.example.android.architecture.blueprints.todoapp.data.source.local.room

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import android.content.Context
import com.example.android.architecture.blueprints.todoapp.data.model.CartModel

@Database(entities = [(CartModel::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun chartDao(): ChartDao

    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also {
                    INSTANCE = it
                }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java, "AndroidApp.db")
                .allowMainThreadQueries()
                .build()
    }
}