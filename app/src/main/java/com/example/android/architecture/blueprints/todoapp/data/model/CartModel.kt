package com.example.android.architecture.blueprints.todoapp.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "chart")
data class CartModel(
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null,
    @SerializedName("product_id")
    @ColumnInfo(name = "product_id")
    var productId: String? = "-",
    @SerializedName("product_name")
    @ColumnInfo(name = "product_name")
    var productName: String? = "-",
    @SerializedName("product_unit")
    @ColumnInfo(name = "product_unit")
    var productUnit: String? = "-",
    @SerializedName("product_price")
    @ColumnInfo(name = "product_price")
    var productPrice: String? = "-",
    @SerializedName("product_sub_total")
    @ColumnInfo(name = "product_sub_total")
    var productSubTotal: String? = "-"
)