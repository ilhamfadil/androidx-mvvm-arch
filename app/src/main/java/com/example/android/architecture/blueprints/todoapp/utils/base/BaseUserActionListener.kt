package com.example.android.architecture.blueprints.todoapp.utils.base

interface BaseUserActionListener {

    fun onRefreshPage()

}