package com.example.android.architecture.blueprints.todoapp.utils.helper

import android.app.AlertDialog
import android.content.Context
import com.example.android.architecture.blueprints.todoapp.R

object AppDialog {

    fun showDialogInformation(context: Context, message: String): AlertDialog {
        val dialog = context.let {
            val builder = AlertDialog.Builder(it)
            builder.setMessage(message)
            builder.setIcon(R.drawable.icon_information)
            builder.create()
        }
        return dialog
    }

}