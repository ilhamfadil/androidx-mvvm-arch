package com.example.android.architecture.blueprints.todoapp.presentation.listdata;

import android.app.Application
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.databinding.ObservableList
import androidx.recyclerview.widget.RecyclerView
import com.example.android.architecture.blueprints.todoapp.utils.base.BaseViewModel
import com.example.android.architecture.blueprints.todoapp.utils.helper.SingleLiveEvent

class ListDataViewModel(context: Application) : BaseViewModel(context) {

    val bListDataList: ObservableList<ListDataModel> = ObservableArrayList()
    val bTextButton = ObservableField("Tambah Data")
    val bIncrement = ObservableField(1)
    val eventClickItem = SingleLiveEvent<ListDataModel>()

    override fun start() {
        super.start()
        loadData()
    }


    fun loadData() {
        bListDataList.clear()
        bListDataList.add(ListDataModel("Radhika"))
        bListDataList.add(ListDataModel("Yusuf"))
        bListDataList.add(ListDataModel("Alifiansyah"))
        bListDataList.add(ListDataModel("GITS Indonesia"))
    }


    companion object {
        // TODO move this function to BaseBinding class
        // function for bindingsList

        @BindingAdapter("app:listDataListData")
        @JvmStatic
        fun setListDataListData(recyclerView: RecyclerView, data: List<ListDataModel>) {
            with(recyclerView.adapter as ListDataAdapter) {
                replaceData(data)
            }
        }
    }

}