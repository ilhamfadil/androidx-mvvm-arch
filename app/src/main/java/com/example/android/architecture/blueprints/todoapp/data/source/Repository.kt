package com.example.android.architecture.blueprints.todoapp.data.source

import com.example.android.architecture.blueprints.todoapp.data.model.CartModel

class Repository(private val remoteDataSource: DataSource, private val localDataSource: DataSource) : DataSource {

    override fun getListCart(refresh: Boolean, callback: DataSource.GetCartCallback) {
        if (!refresh) {
            localDataSource.getListCart(callback = callback)
        }

        remoteDataSource.getListCart(refresh, object : DataSource.GetCartCallback {
            override fun onSuccess(data: List<CartModel>) {
                saveListCart(data)
                localDataSource.getListCart(false, callback)
            }

            override fun onError(errorMessage: String) {
                callback.onError(errorMessage)
            }

            override fun onFinish() {
                callback.onFinish()
            }

        })
    }

    override fun saveListCart(data: List<CartModel>) {
        localDataSource.saveListCart(data)
    }

}