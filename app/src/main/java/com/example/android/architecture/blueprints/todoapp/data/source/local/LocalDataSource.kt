package com.example.android.architecture.blueprints.todoapp.data.source.local

import android.content.Context
import com.example.android.architecture.blueprints.todoapp.data.model.CartModel
import com.example.android.architecture.blueprints.todoapp.data.source.DataSource
import com.example.android.architecture.blueprints.todoapp.data.source.local.room.AppDatabase
import com.example.android.architecture.blueprints.todoapp.data.source.local.room.ChartDao

class LocalDataSource(context: Context) : DataSource {

    private val mHeroesTable: ChartDao by lazy {
        AppDatabase.getInstance(context).chartDao()
    }

    override fun saveListCart(data: List<CartModel>) {
        mHeroesTable.deleteAllChart()
        for (model in data) {
            mHeroesTable.insertChart(model)
        }
    }

    override fun getListCart(refresh: Boolean, callback: DataSource.GetCartCallback) {
        try {
            val data = (mHeroesTable.retrievedChart() ?: arrayListOf())
            if (data.isNotEmpty()) {
                callback.onSuccess(data)
            }
        } catch (e: Exception) {
            callback.onError(e.message ?: e.localizedMessage)
        }

    }


}