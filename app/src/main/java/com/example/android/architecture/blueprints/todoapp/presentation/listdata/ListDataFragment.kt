package com.example.android.architecture.blueprints.todoapp.presentation.listdata


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.android.architecture.blueprints.todoapp.R
import com.example.android.architecture.blueprints.todoapp.databinding.FragmentListDataBinding
import com.example.android.architecture.blueprints.todoapp.utils.base.BaseFragment
import com.example.android.architecture.blueprints.todoapp.utils.chocohelper.ChocoBinding
import com.example.android.architecture.blueprints.todoapp.utils.chocohelper.ChocoChips
import com.example.android.architecture.blueprints.todoapp.utils.chocohelper.ChocoViewModel
import com.example.android.architecture.blueprints.todoapp.utils.helper.AppDialog

class ListDataFragment : BaseFragment<ListDataViewModel>(), ListDataUserActionListener {

    @ChocoBinding(R.layout.fragment_list_data)
    lateinit var mViewDataBinding: FragmentListDataBinding

    @ChocoViewModel
    lateinit var mViewModel: ListDataViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        ChocoChips.inject<FragmentListDataBinding, ListDataViewModel, ListDataUserActionListener>(this)
        mViewDataBinding.mViewModel = mViewModel
        mViewDataBinding.mListener = this@ListDataFragment
        return mViewDataBinding.root
    }

    override fun setContentData() {
        mViewDataBinding.recyclerListData.adapter = ListDataAdapter(mViewModel.bListDataList, mViewModel)
        mViewDataBinding.recyclerListData.layoutManager = LinearLayoutManager(context)
    }

    override fun onCreateObserver(viewModel: ListDataViewModel) {
        viewModel.apply {
            eventClickItem.observe(this@ListDataFragment, Observer {
                if (it != null) {
                    onClickItem(it)
                }
            })
        }
    }

    override fun onClickItem(data: ListDataModel) {
        mViewModel.showMessage.value = data.title
    }

    override fun onRefreshPage() {
        //TODO do refresh page        
    }

    override fun addNewItem() {
        mViewModel.bListDataList.add(ListDataModel("New Data ${mViewModel.bIncrement.get()}"))
        mViewModel.bIncrement.set(mViewModel.bIncrement.get()?.inc())
        AppDialog.showDialogInformation(requireContext(), "Data Addedd").show()
    }

    override fun setMessageType(): String {
        return MESSAGE_TYPE_SNACK_CUSTOM
    }

    override fun onDestroyObserver(viewModel: ListDataViewModel) {
        viewModel.apply {
            eventClickItem.removeObservers(this@ListDataFragment)
        }
    }


    companion object {
        fun newInstance() = ListDataFragment().apply {

        }

    }

}