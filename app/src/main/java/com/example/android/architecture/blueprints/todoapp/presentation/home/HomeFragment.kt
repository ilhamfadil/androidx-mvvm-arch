package com.example.android.architecture.blueprints.todoapp.presentation.home


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.android.architecture.blueprints.todoapp.R
import com.example.android.architecture.blueprints.todoapp.databinding.FragmentHome2Binding
import com.example.android.architecture.blueprints.todoapp.utils.base.BaseFragment
import com.example.android.architecture.blueprints.todoapp.utils.chocohelper.ChocoBinding
import com.example.android.architecture.blueprints.todoapp.utils.chocohelper.ChocoChips
import com.example.android.architecture.blueprints.todoapp.utils.chocohelper.ChocoViewModel


class HomeFragment : BaseFragment<HomeViewModel>(), HomeUserActionListener {

    @ChocoBinding(R.layout.fragment_home2)
    lateinit var mViewDataBinding: FragmentHome2Binding

    @ChocoViewModel
    lateinit var mViewModel: HomeViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        ChocoChips.inject<FragmentHome2Binding, HomeViewModel, HomeUserActionListener>(this)
        mViewDataBinding.mViewModel = mViewModel
        mViewDataBinding.mListener = this@HomeFragment
        return mViewDataBinding.root
    }

    override fun setContentData() {

    }

    override fun onCreateObserver(viewModel: HomeViewModel) {

    }

    override fun onRefreshPage() {

    }

    override fun setMessageType(): String {
        return MESSAGE_TYPE_SNACK_CUSTOM
    }

    override fun onDestroyObserver(viewModel: HomeViewModel) {

    }

    override fun onClickTest() {
        (requireActivity() as HomeActivity).openListData()
    }

    companion object {
        fun newInstance() = HomeFragment().apply {

        }

    }

}
